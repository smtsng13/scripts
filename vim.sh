#!bin/bash                                                                  

mkdir ~/.vim ~/.vim/autoload ~/.vim/bundle

cd ~/.vim/autoload/
wget https://raw.githubusercontent.com/tpope/vim-pathogen/master/autoload/pathogen.vim

cd ~/.vim/bundle/
git clone https://github.com/airblade/vim-gitgutter
git clone https://github.com/ctrlpvim/ctrlp.vim
git clone https://github.com/easymotion/vim-easymotion
git clone https://github.com/ervandew/supertab
git clone https://github.com/godlygeek/tabular
git clone https://github.com/jistr/vim-nerdtree-tabs
git clone https://github.com/majutsushi/tagbar
git clone https://github.com/morhetz/gruvbox
git clone https://github.com/pangloss/vim-javascript
git clone https://github.com/ryanoasis/vim-devicons
git clone https://github.com/scrooloose/nerdtree
git clone https://github.com/scrooloose/nerdcommenter
git clone https://github.com/scrooloose/syntastic
git clone https://github.com/sirver/ultisnips
git clone https://github.com/tiagofumo/vim-nerdtree-syntax-highlight
git clone https://github.com/tomasr/molokai
git clone https://github.com/tpope/vim-fugitive
git clone https://github.com/tpope/vim-repeat
git clone https://github.com/tpope/vim-surround
git clone https://github.com/vim-airline/vim-airline
git clone https://github.com/vim-airline/vim-airline-themes
